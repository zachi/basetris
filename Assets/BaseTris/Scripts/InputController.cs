﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour
{
    #if UNITY_STANDALONE || UNITY_EDITOR
    //for testing
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            BaseTrisMainController.Instance.TryMove(new Vector3(1f, 0f));
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            BaseTrisMainController.Instance.TryMove(new Vector3(-1f, 0f));
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            BaseTrisMainController.Instance.FallFast();
        }
        else if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            BaseTrisMainController.Instance.TryRotate();
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            BaseTrisMainController.Instance.Restart();
        }
    }
    #endif

}
