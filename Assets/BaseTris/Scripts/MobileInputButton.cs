﻿using UnityEngine;
using System.Collections;

public class MobileInputButton : MonoBehaviour
{
    #region Helper enum

    public enum InputValue : int
    {
        Key_Right = 0,
        Key_Left = 1,
        Key_Restart = 2,
        Key_Bottom = 3,
        Key_Rotate = 4
    }

    #endregion

    #region Fields visible in inspector

    public InputValue KeyType;

    #endregion

    #region MonoBehaviour methods

    void OnMouseDown()
    {
        BaseTrisMainController ctrl = BaseTrisMainController.Instance;
        if (this.KeyType == InputValue.Key_Left)
        {
            ctrl.TryMove(new Vector3(-1, 0));
        }
        else if (this.KeyType == InputValue.Key_Right)
        {
            ctrl.TryMove(new Vector3(1, 0));
        }
        else if (this.KeyType == InputValue.Key_Bottom)
        {
            ctrl.FallFast();
        }
        else if (this.KeyType == InputValue.Key_Restart)
        {
            ctrl.Restart();
        }
        else if (this.KeyType == InputValue.Key_Rotate)
        {
            ctrl.TryRotate();
        }
    }

    #endregion
}
