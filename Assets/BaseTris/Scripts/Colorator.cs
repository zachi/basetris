﻿using UnityEngine;
using System.Collections;

public class Colorator : MonoBehaviour
{

    #region Consts

    const float FadeFactor = 1.5f;

    #endregion

    #region Available from Inspector

    public Color32[] colors;
    public bool FadeOnDropDown = true;

    #endregion

    #region Methods

    public void MakeBeauty(SpriteRenderer[] sprites)
    {
        Color32 c = this.colors[UnityEngine.Random.Range(0, this.colors.Length)];
        for (int i = 0; i < sprites.Length; i++)
        {
            //sprites[i].color = c;
            sprites[i].color = c;
        }
    }

    public void Fade(SpriteRenderer s)
    {
        s.color = new Color(
            s.color.r * FadeFactor,
            s.color.g * FadeFactor,
            s.color.b * FadeFactor,
            1f
        );
    }

    #endregion
}

