﻿using UnityEngine;
using System.Collections;

public class BaseTrisMainController : MonoBehaviour
{
    #region Consts

    internal const int WellWidthPlus = 2;
    internal const int WellDepthPlus = 5;

    internal const int WellWidthDefault = 12;
    internal const int WellDepthDefault = 20;

    #endregion

    #region Singleton implementation

    private static BaseTrisMainController _instance;

    public static BaseTrisMainController Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        _instance = this;
    }

    #endregion

    #region Fields

    #region Internal Code Fields

    protected int[,] _mainBoard;
    protected GameObject _currentBlock;
    protected bool _gameOver = false;
    protected bool _pause = true;
    protected int _points = 0;
    protected Vector3 _defaultSpawnPoint;
    protected GameObject _wellGameObject = null;
    protected Colorator colorator;

    #endregion

    #region Inspector-visible fields

    public float FallingDelay = .7f;
    public GameObject[] BlockPrefabs;
    public GameObject WallObject;
    public bool DontRotateSprites = true;

    #endregion

    #endregion

    #region Methods

    void Start()
    {
        this.colorator = this.GetComponent<Colorator>();
        this.GameInit();
    }

    protected void GameInit()
    {
        this.ClearGame();
        this.GenerateBoard();
        this.PrepareGame();
        this.StartGame();
    }

    protected void ClearGame()
    {
        if (this._currentBlock != null)
        {
            GameObject.Destroy(this._currentBlock);
        }
        GameObject.Destroy(_wellGameObject);
        this._currentBlock = null;
        this._wellGameObject = null;
    }

    protected virtual void GenerateBoard(int width = WellWidthDefault, int depth = WellDepthDefault)
    {
        this._wellGameObject = new GameObject();
        this._wellGameObject.name = "Well";
        this._mainBoard = new int[width + WellWidthPlus, depth + WellDepthPlus];
        for (int x = 0; x < this._mainBoard.GetLength(0); x++)
        {
            for (int y = 0; y < this._mainBoard.GetLength(1); y++)
            {
                if (x == 0 ||
                    x == this._mainBoard.GetLength(0) - 1 ||
                    y == 0)
                {
                    this._mainBoard[x, y] = 1;
                    GameObject w = (GameObject)GameObject.Instantiate(WallObject, new Vector3(x, y, 0), Quaternion.identity);
                    w.transform.SetParent(this._wellGameObject.transform);
                }
                else
                {
                    this._mainBoard[x, y] = 0;
                }
            }
        }
    }

    protected virtual void PrepareGame()
    {
        this._gameOver = false;
        this._pause = true;
        this._points = 0;
        this._defaultSpawnPoint = new Vector3(Mathf.FloorToInt(this._mainBoard.GetLength(0) / 2), this._mainBoard.GetLength(1) - 4, 0);
    }

    protected virtual void StartGame()
    {
        this._pause = false;
        this._currentBlock = GenerateBlock(ref this._defaultSpawnPoint);
        this.EnableFalling();
    }

    private GameObject GenerateBlock(ref Vector3 spawnPoint)
    {
        int blockId = UnityEngine.Random.Range(0, this.BlockPrefabs.Length);
        GameObject _generated = (GameObject)GameObject.Instantiate(this.BlockPrefabs[blockId], spawnPoint, Quaternion.identity);
        BlockModel block = _generated.GetComponent<BlockModel>();
        SpriteRenderer[] s = new SpriteRenderer[block.bricks.Length];
        for (int i = 0; i < block.bricks.Length; i++)
        {
            if (this.PlaceOccupied(block.bricks[i].transform.position))
            {
                return null; // Falling methods will invoke GameOver
            }
            s[i] = block.bricks[i].gameObject.GetComponent<SpriteRenderer>();
        }
        colorator.MakeBeauty(s);
        return _generated;
    }

    private void DissolveBlock(BlockModel block)
    {
        for (int i = 0; i < block.bricks.Length; i++)
        {
            block.bricks[i].SetParent(this._wellGameObject.transform);
        }
        GameObject.Destroy(block.gameObject);
    }

    private bool CanMoveInDirection(Vector3 currentPosition, Vector3 direction)
    {
        Vector2 summed = currentPosition + direction;
        return !(this.PlaceOccupied(summed));
    }

    private bool PlaceOccupied(Vector2 positionOnBoard)
    {
        if (positionOnBoard.y >= this._mainBoard.GetLength(1))
        {
            return false;
        }
        return this._mainBoard[Mathf.RoundToInt(positionOnBoard.x), Mathf.RoundToInt(positionOnBoard.y)] > 0;
    }

    protected void EnableFalling()
    {
        this.InvokeRepeating("Falling", this.FallingDelay, this.FallingDelay);
    }

    protected void DisableFalling()
    {
        this.CancelInvoke();
    }

    private void Falling()
    {
        if (this._currentBlock != null)
        {
            BlockModel block = this._currentBlock.GetComponent<BlockModel>();
            //will block hit something?
            for (int i = 0; i < block.bricks.Length; i++)
            {
                if (!this.CanMoveInDirection(block.bricks[i].position, new Vector2(0f, -1f)))
                {
                    this.BlockHitFloor(block);
                    return;
                }
            }
            this._currentBlock.transform.position = this._currentBlock.transform.position + new Vector3(0f, -1f);
        }
        else
        {
            this._gameOver = true;
            this._pause = true;
            this.DisableFalling();
            Debug.Log("GameOver");
        }
    }

    private void BlockHitFloor(BlockModel block = null)
    {
        if (block == null)
        {
            block = this._currentBlock.GetComponent<BlockModel>();
        }
        //mark _mainBoard filled
        for (int j = 0; j < block.bricks.Length; j++)
        {
            this._mainBoard[
                Mathf.RoundToInt(block.bricks[j].position.x),
                Mathf.RoundToInt(block.bricks[j].position.y)
            ] = 1;
            this.colorator.Fade(block.bricks[j].gameObject.GetComponent<SpriteRenderer>());
        }
        this.DissolveBlock(block);
        this.CheckLines();
        //if any brick is over well depth - gameover
        for (int i = 0; i < block.bricks.Length; i++)
        {
            if (block.bricks[i].position.y > this._mainBoard.GetLength(1) - WellDepthPlus)
            {
                this._currentBlock = null;
                return;
            }
        }
        this._currentBlock = GenerateBlock(ref this._defaultSpawnPoint);
    }

    private void CheckLines()
    {
        for (int i = 1; i < this._mainBoard.GetLength(1); i++)
        {
            while (this.CheckLine(i))
            {
                this.DestroyLine(i);
            }
        }  
    }

    private bool CheckLine(int lineIdx)
    {
        for (int i = 1; i < this._mainBoard.GetLength(0) - 1; i++)
        {
            if (this._mainBoard[i, lineIdx] == 0)
            {
                return false;
            }
        }
        return true;
    }

    private void DestroyLine(int lineIdx)
    {
        Collider2D[] colliders = Physics2D.OverlapAreaAll(
                                     new Vector2(1, -.5f + lineIdx),
                                     new Vector2(this._mainBoard.GetLength(0) - 2, lineIdx));
        for (int i = 0; i < colliders.Length; i++)
        {
            GameObject.Destroy(colliders[i].gameObject);
        }
        colliders = Physics2D.OverlapAreaAll(
            new Vector2(1, lineIdx),
            new Vector2(this._mainBoard.GetLength(0) - 2, this._mainBoard.GetLength(1) - 1));
        for (int i = 0; i < colliders.Length; i++)
        {
            colliders[i].transform.position = colliders[i].transform.position + new Vector3(0, -1);
        }
        for (int i = 1; i < this._mainBoard.GetLength(0) - 1; i++)
        {
            for (int j = lineIdx; j < this._mainBoard.GetLength(1) - 2; j++)
            {
                this._mainBoard[i, j] = this._mainBoard[i, j + 1];
            }
        }

    }

    #region Methods called elsewhere

    public bool TryMove(Vector3 direction)
    {
        BlockModel block = this._currentBlock.GetComponent<BlockModel>();
        for (int i = 0; i < block.bricks.Length; i++)
        {
            if (!(CanMoveInDirection(block.bricks[i].transform.position, direction)))
            {
                return false;
            }
        }
        block.transform.position = block.transform.position + direction;
        return true;
    }

    /// <summary>
    /// Happens when down-arrow was pressed,
    /// block immediately hits the ground
    /// </summary>
    public void FallFast()
    {
        while (this.TryMove(new Vector3(0f, -1f)))
            ;
        this.BlockHitFloor();
    }

    public bool TryRotate()
    {
        BlockModel block = this._currentBlock.GetComponent<BlockModel>();
        block.transform.RotateAround(block.pivot, Vector3.forward, 90f);
        for (int i = 0; i < block.bricks.Length; i++)
        { 
            if (this.PlaceOccupied(block.bricks[i].transform.position))
            {
                block.transform.RotateAround(block.pivot, Vector3.forward, -90f);
                return false;
            }
        }
        if (DontRotateSprites)
        {
            for (int i = 0; i < block.bricks.Length; i++)
            {
                block.bricks[i].transform.RotateAround(block.bricks[i].transform.position, Vector3.forward, -90f);
            }
        }
        return true;
    }

    public void Restart()
    {
        this.CancelInvoke();
        this.GameInit();
    }

    #endregion

    #endregion

}
 