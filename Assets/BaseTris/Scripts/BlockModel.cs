﻿using UnityEngine;
using System.Collections;

public class BlockModel : MonoBehaviour
{

    #region Fields

    private Transform pivotTransform;

    #region Fields available in inspector

    public Transform[] bricks;

    #endregion

    #endregion

    #region Properties

    public Vector3 pivot
    {
        get { return this.pivotTransform.position; }
    }

    #endregion

    #region MonoBehaviour methods

    void Start()
    {
        this.pivotTransform = this.transform.FindChild("RotatePivot");
    }

    #endregion
}
